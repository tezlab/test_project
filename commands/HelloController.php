<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\Food;
use app\models\Ingredient;
use app\models\FoodIngredient;
/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        $this->genData();
        $this->genFoodIng();
    }
    
    public function genData(){
        for($i = 0; $i < 100; $i++){
            $model = new Ingredient;
            $model->name = 'Ingredient ' . $i;
            if($model->save())
                echo "Ingredient saved\n";
        }
        
        for($i = 0; $i < 30; $i++){
            $model = new Food;
            $model->name = 'Food '. $i;
            if($model->save())
                echo "Food saved\n";
        }        
    }
    
    public function genFoodIng(){        
        $foods = Food::find()->all();
        foreach($foods as $food){
            $limit = rand(5, 12);
            $ings = Ingredient::find()->orderby('rand()')->limit($limit)->all();
            foreach($ings as $ing){
                $food_ing = new FoodIngredient;
                $food_ing->food_id = $food->id;
                $food_ing->ingredient_id = $ing->id;
                if($food_ing->save())
                    echo "Ingredient added to food\n";
            }
        }
    }
}
