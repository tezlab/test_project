<?php

namespace app\controllers;

use Yii;
use app\models\Food;
use app\models\FoodIngredient;
use app\models\FoodSearch;
use app\models\Ingredient;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\db\Query;
/**
 * FoodController implements the CRUD actions for Food model.
 */
class FoodController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Food models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FoodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Food model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Food model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Food();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Food model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Food model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionFind(){
        $ingredients = $_POST['ingredients'];
        
        // Пример SQL-запроса для игнредиентов с ID ('2', '3', '6', '8')
        // SELECT `food_id`, count(food_id) as cnt 
        // FROM `food_ingredient` fi 
        // LEFT JOIN `ingredient` ON fi.ingredient_id = ingredient.id 
        // WHERE (`ingredient_id` IN ('2', '3', '6', '8')) AND 
        // (SELECT COUNT(*) 
        //     FROM `food_ingredient` fi2 
        //     LEFT JOIN `ingredient` i ON fi2.ingredient_id = i.id 
        //     WHERE fi.food_id=fi2.food_id AND i.status=0)=0 
        // GROUP BY `food_id` 
        // HAVING cnt > 1 
        // ORDER BY `cnt` DESC
        
        if(count($ingredients) < 2)
            return $this->renderPartial('_more');
            
        $check_hidden = (new Query)
        ->select('count(*)')
        ->from('food_ingredient fi2')
        ->leftJoin('ingredient i2', 'fi2.ingredient_id = i2.id')
        ->where('fi.food_id=fi2.food_id AND i2.status=0')->createCommand()->getRawSql();
        
        $models = FoodIngredient::find()
        ->from('food_ingredient fi')
        ->leftJoin('ingredient i', 'fi.ingredient_id = i.id')
        ->select('food_id, count(food_id) as cnt')
        ->where(['and', ['in', 'ingredient_id', $ingredients], ['('.$check_hidden . ')' => 0]])
        ->groupBy('food_id')
        ->having('cnt = 5')
        ->all();
        
        if(empty($models)){
            $models = FoodIngredient::find()
            ->from('food_ingredient fi')
            ->leftJoin('ingredient i', 'fi.ingredient_id = i.id')
            ->select('food_id, count(food_id) as cnt')
            ->where(['and', ['in', 'ingredient_id', $ingredients], ['('.$check_hidden . ')' => 0]])
            ->groupBy('food_id')
            ->having('cnt > 1')
            ->orderBy('cnt DESC')
            ->all();
        }
        
        $ingredient_names = ArrayHelper::getColumn(Ingredient::find()->select('name')->where(['in', 'id', $ingredients])->asArray()->all(), 'name');
        
        return $this->renderPartial('_results', ['models' => $models, 'ingredients' => $ingredient_names]);        
    }

    /**
     * Finds the Food model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Food the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Food::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
