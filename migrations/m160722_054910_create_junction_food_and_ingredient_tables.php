<?php

use yii\db\Migration;

/**
 * Handles the creation for table `food_ingredient`.
 * Has foreign keys to the tables:
 *
 * - `food`
 * - `ingredient`
 */
class m160722_054910_create_junction_food_and_ingredient_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('food_ingredient', [
            'food_id' => $this->integer(),
            'ingredient_id' => $this->integer(),
            'PRIMARY KEY(food_id, ingredient_id)',
        ]);

        // creates index for column `food_id`
        $this->createIndex(
            'idx-food_ingredient-food_id',
            'food_ingredient',
            'food_id'
        );

        // add foreign key for table `food`
        $this->addForeignKey(
            'fk-food_ingredient-food_id',
            'food_ingredient',
            'food_id',
            'food',
            'id',
            'CASCADE'
        );

        // creates index for column `ingredient_id`
        $this->createIndex(
            'idx-food_ingredient-ingredient_id',
            'food_ingredient',
            'ingredient_id'
        );

        // add foreign key for table `ingredient`
        $this->addForeignKey(
            'fk-food_ingredient-ingredient_id',
            'food_ingredient',
            'ingredient_id',
            'ingredient',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `food`
        $this->dropForeignKey(
            'fk-food_ingredient-food_id',
            'food_ingredient'
        );

        // drops index for column `food_id`
        $this->dropIndex(
            'idx-food_ingredient-food_id',
            'food_ingredient'
        );

        // drops foreign key for table `ingredient`
        $this->dropForeignKey(
            'fk-food_ingredient-ingredient_id',
            'food_ingredient'
        );

        // drops index for column `ingredient_id`
        $this->dropIndex(
            'idx-food_ingredient-ingredient_id',
            'food_ingredient'
        );

        $this->dropTable('food_ingredient');
    }
}
