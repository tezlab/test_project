$(document).ready(function(){
    $('body').on('submit', '#find-food', function(e){
        e.preventDefault();
        console.log($('#ingredients').val());
        $.ajax({
            url: $(this).attr('action'),
            method: 'POST',
            data: {ingredients: $('#ingredients').val()},
            success: function(data){
                $('.body-content .row').html(data);
            }
        })
    })
});