<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Ingredient;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Food */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="food-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?php
        echo Select2::widget([
            'name' => 'ingredients',
            'value' => $model->getIngredientsId(),
            'data' => ArrayHelper::map(Ingredient::find()->all(), 'id', 'name'),
            'options' => ['multiple' => true, 'placeholder' => 'Select ingredients ...']
        ]); 
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
