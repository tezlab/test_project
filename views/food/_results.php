<div class="panel panel-default">
    <div class="panel-heading">Результаты</div>
    <div class="panel-body">           
    <?php if(!empty($models)): foreach($models as $item):?>
    <div class="well-sm">
        <h3><?php echo $item->food->name;?></h3>
        <p>
            <?php $food_ingredients = $item->food->getIngredientsName(); foreach($food_ingredients as $ing):?>
            <span class="label label-<?= count(array_diff($ingredients, [$ing])) == count($ingredients) ? 'default' : 'success'?>"><?= $ing?></span>
            <?php endforeach;?>
        </p>
    </div>
    <?php endforeach; else: ?>
    <div class="well">
        <h3>Ничего не найдено</h3>
    </div>
    <?php endif;?>
    </div>
</div>