<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Ingredient;
use kartik\select2\Select2;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Выберите инредиенты!</h1>

        <p class="lead">Необходимо выбрать не менее 2 и не более 5 ингредиентов.</p>

        
        <?php $form = ActiveForm::begin(['action' => '/food/find', 'id' => 'find-food']); ?>
        <?php
            echo Select2::widget([
                'name' => 'ingredients',
                'value' => '',
                'data' => ArrayHelper::map(Ingredient::find()->where(['status' => 1])->all(), 'id', 'name'),
                'toggleAllSettings' => [
                    'selectOptions' => ['class' => 'disable hidden'],
                ],
                'options' => ['multiple' => true, 'id' => 'ingredients', 'placeholder' => 'Select ingredients ...'],
                'pluginOptions' => [
                    'maximumSelectionLength' => 5
                ]
            ]); 
        ?>
        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <div class="body-content">

        <div class="row">
            
        </div>

    </div>
</div>
