<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "food".
 *
 * @property integer $id
 * @property string $name
 *
 * @property FoodIngredient[] $foodIngredients
 * @property Ingredient[] $ingredients
 */
class Food extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     
    public $ingredients;
    public static function tableName()
    {
        return 'food';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodIngredients()
    {
        return $this->hasMany(FoodIngredient::className(), ['food_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredients()
    {
        return $this->hasMany(Ingredient::className(), ['id' => 'ingredient_id'])->viaTable('food_ingredient', ['food_id' => 'id']);
    }
    
    public function afterSave($insert, $changedAttributes){
        if(isset($_POST['ingredients'])){
            FoodIngredient::deleteAll(['food_id' => $this->id]);
            foreach($_POST['ingredients'] as $item){
                $model = new FoodIngredient;
                $model->food_id = $this->id;
                $model->ingredient_id = $item;
                $model->save();
            }                        
        }
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function getIngredientsId(){
        return ArrayHelper::getColumn(FoodIngredient::find()->select('ingredient_id')->where(['food_id' => $this->id])->asArray()->all(), 'ingredient_id');
    }
    
    public function getIngredientsName(){
        return ArrayHelper::getColumn(FoodIngredient::find()->leftJoin('ingredient', 'food_ingredient.ingredient_id = ingredient.id')->select('ingredient.name')->where(['food_id' => $this->id])->asArray()->all(), 'name');
    }
    
    public function beforeDelete(){
        FoodIngredient::deleteAll(['food_id' => $this->id]);
        return parent::beforeDelete();
    }
}
